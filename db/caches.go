package db

import (
	"errors"

	"github.com/jinzhu/gorm"
)

//
type ConnCache struct {
	connections map[uint]*Connection
	db          *gorm.DB
}

//
var NotFound error = errors.New("value missing")

//
func (cache *ConnCache) Get(id uint) (*Connection, error) {
	if val := cache.connections[id]; val != nil {
		return val, nil
	}
	var conn Connection
	cache.db.First(&conn, id)
	if conn.ID == 0 {
		return nil, NotFound
	}
	cache.connections[conn.ID] = &conn
	return &conn, nil
}

//
func (cache *ConnCache) Drop(id uint) {
	if val := cache.connections[id]; val != nil {
		cache.connections[id] = nil
	}
}
