package db

import (
	"github.com/jinzhu/gorm"
)

//
type Connection struct {
	gorm.Model
	SutHost      string
	SutPort      int
	OuterHost    *string
	OuterPort    *int
	SutInitiator bool
	HeaderLength int
	HeaderType   int
	Enabled      bool
}
