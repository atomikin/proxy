package tcp

import (
	"encoding/binary"
	"strconv"
	"time"

	"bitbucket.org/atomikin/proxy/db"
)

// Module - tcp module
type Module struct {
	SystemUnderTest NodeInterface
	OuterSytem      NodeInterface
	Info            *db.Connection
	ServiceIn       chan ServiceMessage
}

const (
	// TimeOut - common TO for all operations
	TimeOut = 2 * time.Second
)

func getHandler(handlerLen int, handerType int) Handler {
	if handerType == 0 {
		if handlerLen == 2 {
			return func(data []byte) (int, error) {
				return int(binary.BigEndian.Uint16(data)), nil
			}
		} else if handlerLen == 4 {
			return func(data []byte) (int, error) {
				return int(binary.BigEndian.Uint16(data[:2])), nil
			}
		}
	} else if handerType == 1 {
		return func(data []byte) (int, error) {
			return strconv.Atoi(string(data))
		}
	} else if handerType == 2 {
		return func(data []byte) (int, error) {
			handler := getHandler(handlerLen, 0)
			l, err := handler(data)
			if err != nil {
				return 0, err
			}
			return l - handlerLen, nil
		}
	}

	return nil
}

//
func MakeModule(info *db.Connection) *Module {
	module := new(Module)
	module.Info = info
	module.ServiceIn = make(chan ServiceMessage)
	if info.SutInitiator {
		module.SystemUnderTest = MakeServerNode(
			info.SutHost, string(info.SutPort),
			info.HeaderLength, info.HeaderType,
		)
		go RunNode(module.SystemUnderTest)
		WaitForStatus(module.SystemUnderTest, Connecting, TimeOut)

		if info.OuterHost != nil && info.OuterPort != nil {
			module.OuterSytem = MakeClientNode(
				*info.OuterHost, string(*info.OuterPort), info.HeaderLength, info.HeaderType,
			)
			go RunNode(module.OuterSytem)
			WaitForStatus(module.OuterSytem, Connecting, TimeOut)
		}
	}
	return module
}

// This is stub!! API TBD
func askOracle(data *[]byte, module *Module) (*[]byte, NodeInterface) {
	return data, module.OuterSytem
}

// this is a stub of the real function; API - TBD
func askForSave(data *[]byte) {
}

//
func (module *Module) Run() {
	for {
		status, err := GetStatus(module.SystemUnderTest, TimeOut)
		if err != nil || *status == NotRun {
			break
		}
		if module.OuterSytem != nil {
			// when there is an integrational system connected (resender mode)
			select {
			case msg := <-module.SystemUnderTest.GetOut():
				if answer, node := askOracle(msg, module); node != nil {
					status, err := GetStatus(node, TimeOut)
					if err == nil && *status == Connected {
						node.GetIn() <- answer
					}
				}

			case msg := <-module.OuterSytem.GetOut():
				status, err := GetStatus(module.SystemUnderTest, TimeOut)
				if err == nil && *status == Connected {
					askForSave(msg)
					module.SystemUnderTest.GetIn() <- msg

				}
			}
		} else {
			// when there is no outer system (Stub mode only)
			msg := <-module.SystemUnderTest.GetOut()
			if answer, node := askOracle(msg, module); node != nil {
				status, err := GetStatus(node, TimeOut)
				if err == nil && *status == Connected {
					node.GetIn() <- answer
				}
			}
		}

	}
}
