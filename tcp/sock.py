import socket
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('host', type=str, help='host')
    parser.add_argument('port', type=int, help='port')

    args = parser.parse_args()
    sock = socket.socket()
    sock.connect((args.host, args.port))
    sock.recv(2048)

if __name__ == '__main__':
    main()
