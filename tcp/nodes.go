package tcp

import (
	"errors"
	"fmt"
	"net"
	"time"
)

const (
	// NotRun service has been started, though has not established conn
	NotRun = "noRun"
	// Connecting is a status of the, meaning the node is trying to connect
	Connecting = "connecting"
	// Connected is a status meaning that connection has been established and message exchange is in progress
	Connected = "connected"
	empty     = ""
)

var statuses = map[string]interface{}{
	NotRun:     empty,
	Connecting: empty,
	Connected:  empty,
}

// Timeouts
const (
	reconnSleepTime = time.Second
	readDeadline    = 15 * time.Second
)

// buffers
const (
	bufferLen = 1024
)

// ErrClosedByRemote - connection was closed by the remote peer
var ErrClosedByRemote = errors.New("conn closed by remote")
var ErrResetRequested = errors.New("reset was requested")
var ErrUnknownStatus = errors.New("invalid status")
var ErrStop = errors.New("Stopped")
var ErrReset = errors.New("Reset")
var ErrTO = errors.New("TimeOut")

// TIMEOUT = errors.New("Read request timed out")

/*
ServiceMessage is a representation of a service message
*/
type ServiceMessage struct {
	Command  int
	Status   int
	Data     string
	RespChan chan ServiceMessage
}

// commands
const (
	// Status - check status of the node
	Status = iota
	// Reset - close conn and reconnect again
	Reset
	Stop
	PingOnStatus
)

// message statuses
const (
	OK = iota
	Error
)

/*
Node is a representation of the socket
*/
type Node struct {
	Out          chan *[]byte
	In           chan *[]byte
	InService    chan ServiceMessage
	OutService   chan ServiceMessage
	HostName     string
	Port         string
	LenHandler   func([]byte) (int, error)
	HeaderLength int
	Status       string
	// connection *net.Conn
}

/*
 */
func (node *Node) Connect() (*net.Conn, error) {
	panic(errors.New("Not implemented"))
}

/*
NodeInterface is a interface for all nodes
It allows to abstract away from the client-server nature of the certain node
*/
type NodeInterface interface {
	Connect() (*net.Conn, error)
	GetHost() string
	GetPort() string
	GetIn() chan *[]byte
	GetOut() chan *[]byte
	GetServiceIn() chan ServiceMessage
	GetServiceOut() chan ServiceMessage
	HandleLen([]byte) (int, error)
	GetHeaderLength() int
	GetStatus() string
	SetStatus(status string) error
}

/*
ServerNode is a wrapped socket server
*/
type ServerNode struct {
	Node
	listener *net.Listener
}

/*
Connect returns a *net.Conn
*/
func (node *ServerNode) Connect() (*net.Conn, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%s", node.HostName, node.Port))
	if err != nil {
		return nil, err
	}
	defer listener.Close()
	conn, err := listener.Accept()
	if err != nil {
		return nil, err
	}
	return &conn, nil
}

//
func (node *ServerNode) GetHost() string {
	return node.HostName
}

//
func (node *ServerNode) GetPort() string {
	return node.Port
}

//
func (node *ServerNode) GetIn() chan *[]byte {
	return node.In
}

//
func (node *ServerNode) GetOut() chan *[]byte {
	return node.Out
}

//
func (node *ServerNode) GetServiceIn() chan ServiceMessage {
	return node.InService
}

//
func (node *ServerNode) GetServiceOut() chan ServiceMessage {
	return node.OutService
}

//
func (node *ServerNode) HandleLen(data []byte) (int, error) {
	return node.LenHandler(data)
}

//
func (node *ServerNode) GetHeaderLength() int {
	return node.HeaderLength
}

//
func (node *ServerNode) GetStatus() string {
	return node.Status
}

//
func (node *ServerNode) SetStatus(status string) error {
	if statuses[status] != nil {
		node.Status = status
		fmt.Printf("Set status %s\n", status)
		return nil
	}
	return ErrUnknownStatus
}

/*
ClientNode is a wrapped socket client
*/
type ClientNode struct {
	Node
}

//
func (node *ClientNode) GetHost() string {
	return node.HostName
}

//
func (node *ClientNode) GetPort() string {
	return node.Port
}

//
func (node *ClientNode) GetIn() chan *[]byte {
	return node.In
}

//
func (node *ClientNode) GetOut() chan *[]byte {
	return node.Out
}

//
func (node *ClientNode) GetServiceIn() chan ServiceMessage {
	return node.InService
}

//
func (node *ClientNode) GetServiceOut() chan ServiceMessage {
	return node.OutService
}

//
func (node *ClientNode) HandleLen(data []byte) (int, error) {
	return node.LenHandler(data)
}

//
func (node *ClientNode) GetHeaderLength() int {
	return node.HeaderLength
}

//
func (node *ClientNode) GetStatus() string {
	return node.Status
}

//
func (node *ClientNode) SetStatus(status string) error {
	if statuses[status] != nil {
		node.Status = status
		return nil
	}
	return ErrUnknownStatus
}

/*
 */
func (node *ClientNode) Connect() (*net.Conn, error) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", node.HostName, node.Port))
	if err != nil {
		return nil, err
	}
	return &conn, nil
}

/*
 */
func RunNode(node NodeInterface) {
	errchan := make(chan error, 3)
	servicechan := make(chan bool)
	go manager(node, errchan, servicechan)
MainLoop:
	for {
		node.SetStatus(Connecting)
		writerchan := make(chan bool)
		connchan := make(chan *net.Conn)
		var conn *net.Conn
		var err error
		defer func() {
			node.SetStatus(NotRun)
			servicechan <- false
			if conn != nil {
				defer func() {
					writerchan <- false
					(*conn).Close()
					conn = nil
				}()
			}
		}()

		go func() {
			if conn, err = node.Connect(); err != nil {
				time.Sleep(reconnSleepTime)
				errchan <- err
			}
			connchan <- conn
		}()
		for {
			select {
			case err = <-errchan:
				fmt.Printf("Got Err: %s\n", err)
				if err == ErrStop {
					return
				}
				if conn != nil {
					writerchan <- false
					(*conn).Close()
					conn = nil
				}

				fmt.Printf("[NODE ERROR]: %s", err)
				// servicechan <- false
				continue MainLoop
			case conn = <-connchan:
				go reader(node, conn, errchan)
				go writer(node, conn, errchan, writerchan)
			}
		}

	}

}

func reader(node NodeInterface, conn *net.Conn, errchan chan error) {
	defer func() {
		if fatal := recover(); fatal != nil {
			if err, ok := fatal.(error); ok {
				errchan <- err
			}
		}
	}()
	node.SetStatus(Connected)
	fmt.Println("Connected")
	buffer := make([]byte, 0, bufferLen*2)
	readBuffer := make([]byte, bufferLen)
	expectedLen := node.GetHeaderLength()
	readHeader := true
	message := make([]byte, 0, bufferLen)

	for {
		(*conn).SetReadDeadline(time.Now().Add(readDeadline))
		n, readRrr := (*conn).Read(readBuffer)
		fmt.Printf("got raw msg <- %x\n", readBuffer[:n])
		if readRrr != nil {
			if timeoutErr, ok := readRrr.(net.Error); ok && timeoutErr.Timeout() {
				continue
			} else {
				errchan <- readRrr
				return
			}
		}
		if n == 0 {
			errchan <- ErrClosedByRemote
			return
		}
		buffer = append(buffer, readBuffer[:n]...)
		for len(buffer) >= expectedLen {
			data := buffer[:expectedLen]
			message = append(message, data...)
			buffer = buffer[expectedLen:]
			if readHeader {
				var err error
				expectedLen, err = node.HandleLen(data)
				readHeader = !readHeader
				if err != nil {
					// need to log and exit - either header is wrong of message is broken
					errchan <- err
					return
				}
			} else {
				expectedLen = node.GetHeaderLength()
				readHeader = !readHeader
				msg := make([]byte, len(message))
				copy(msg, message)
				node.GetOut() <- &msg
				fmt.Printf("got msg <- %x\n", &msg)
				message = message[:0]
			}
		}
	}
}

func writer(node NodeInterface, conn *net.Conn, errchan chan error, writerchan chan bool) {
	for {
		select {
		case msg := <-node.GetIn():
			fmt.Printf("Sending msg -> %x", *msg)
			if _, err := (*conn).Write(*msg); err != nil {
				errchan <- err
			}
		case <-writerchan:
			break
		}

	}
}

func manager(node NodeInterface, errchan chan error, servicechan chan bool) {
MainLoop:
	for {
		select {
		case msg := <-node.GetServiceIn():
			switch msg.Command {
			case Status:
				msg.RespChan <- ServiceMessage{
					Status:  OK,
					Data:    node.GetStatus(),
					Command: msg.Command,
				}
			case Reset:
				msg.RespChan <- ServiceMessage{
					Status: OK, Data: Connecting, Command: msg.Command,
				}
				errchan <- ErrResetRequested
			case Stop:
				fmt.Println("Got Stop")
				errchan <- ErrStop
				fmt.Printf("Send %s\n", ErrStop)
				msg.RespChan <- ServiceMessage{
					Command: Stop,
					Status:  OK,
					Data:    NotRun,
				}
			case PingOnStatus:
				if statuses[msg.Data] == nil {
					msg.RespChan <- ServiceMessage{
						Status: Error, Data: fmt.Sprintf("Invalid status to wait"),
					}
					continue
				}
				for {
					if node.GetStatus() == msg.Data {
						msg.RespChan <- ServiceMessage{
							Command: msg.Command, Status: OK,
							Data: node.GetStatus(),
						}
						fmt.Printf("Got Status: %s\n", node.GetStatus())
						continue MainLoop
					}
					fmt.Println("waiting...")
					time.Sleep(time.Millisecond)
				}
			default:
				fmt.Printf("Got unknown command: %d\n", msg.Command)
			}
		case <-servicechan:
			fmt.Println("manager got err")
			return
		}

	}
}

// Handler - is a type alias for handler fuction
type Handler func([]byte) (int, error)

/*
 */
func MakeServerNode(host string, port string, lg int, ht int) NodeInterface {
	node := ServerNode{
		Node: Node{
			HostName:     host,
			Port:         port,
			In:           make(chan *[]byte),
			Out:          make(chan *[]byte),
			InService:    make(chan ServiceMessage, 10),
			OutService:   make(chan ServiceMessage),
			HeaderLength: lg,
			LenHandler:   getHandler(lg, ht),
			Status:       NotRun,
		},
	}
	return &node
}

/*
 */
func MakeClientNode(host string, port string, lg int, ht int) NodeInterface {
	node := ClientNode{
		Node: Node{
			HostName:     host,
			Port:         port,
			In:           make(chan *[]byte),
			Out:          make(chan *[]byte),
			InService:    make(chan ServiceMessage),
			OutService:   make(chan ServiceMessage),
			HeaderLength: lg,
			LenHandler:   getHandler(lg, ht),
			Status:       NotRun,
		},
	}
	return &node
}

// WaitForStatus asks node for status and waits for response until TO
func WaitForStatus(node NodeInterface, status string, to time.Duration) (*string, error) {
	tochan := make(chan bool)
	respchan := make(chan ServiceMessage)
	go func() {
		time.Sleep(to)
		tochan <- true
	}()
	if len(node.GetServiceIn()) > 0 {
		return nil, errors.New("cannot stop")
	}
	node.GetServiceIn() <- ServiceMessage{Command: PingOnStatus, Data: status, RespChan: respchan}

	select {
	case <-tochan:
		return nil, ErrTO
	case msg := <-respchan:
		if msg.Status == OK {
			return &msg.Data, nil
		}
	}
	return nil, ErrUnknownStatus
}

// GetStatus - get status with TO
func GetStatus(node NodeInterface, to time.Duration) (*string, error) {
	tochan := make(chan bool)
	respchan := make(chan ServiceMessage)
	go func() {
		time.Sleep(to)
		tochan <- true
	}()
	node.GetServiceIn() <- ServiceMessage{Command: Status, RespChan: respchan}

	select {
	case <-tochan:
		return nil, ErrTO
	case msg := <-respchan:
		if msg.Status == OK {
			return &msg.Data, nil
		}
	}
	return nil, ErrUnknownStatus
}

// StopNode - stop this node and wait for it to stop
func StopNode(node NodeInterface, to time.Duration) (*string, error) {
	status := node.GetStatus()
	if status != NotRun {
		tochan := make(chan bool)
		respchan := make(chan ServiceMessage)
		node.GetServiceIn() <- ServiceMessage{Command: Stop, RespChan: respchan}
		go func() {
			time.Sleep(to)
			tochan <- true
		}()
		select {
		case <-tochan:
			return nil, ErrTO
		case msg := <-respchan:
			if msg.Status == OK {
				return &msg.Data, nil
			}
		}
	}

	return &status, nil

}

// ReconnNode - reconnect this node and wait for it to restart
func ReconnNode(node NodeInterface, to time.Duration) (*string, error) {
	status := node.GetStatus()
	if status != NotRun {
		tochan := make(chan bool)
		respchan := make(chan ServiceMessage)
		node.GetServiceIn() <- ServiceMessage{Command: Reset, RespChan: respchan}
		go func() {
			time.Sleep(to)
			tochan <- true
		}()
		select {
		case <-tochan:
			return nil, ErrTO
		case msg := <-respchan:
			if msg.Status == OK {
				return &msg.Data, nil
			}
		}
	}
	return &status, nil

}
