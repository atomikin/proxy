package tcp

import (
	"fmt"
	"os/exec"
	"testing"
	"time"
)

var timeOut = 1 * time.Second

func TestServerNodeBasic(t *testing.T) {
	fmt.Println("Run test")
	for _, node := range []NodeInterface{
		MakeServerNode("localhost", "8080", 2, 0),
		MakeClientNode("localhost", "8081", 2, 0),
	} {
		go RunNode(node)
		defer func() {
			_, err := StopNode(node, timeOut)
			if err != nil {
				t.Fatalf(err.Error())
			}
		}()
		if r, err := GetStatus(node, timeOut); err != nil || *r != Connecting {
			if err != nil {
				t.Fatal(err.Error())
			} else {
				t.Errorf("Status should be Connecting, got %s", *r)
			}
		}
		// fmt.Printf("Status: %s\n", node.GetStatus())
		fmt.Println("Run status: OK")
		if resp, err := StopNode(node, timeOut); err != nil || *resp != NotRun {
			status := *resp
			if err != nil {
				t.Error(err)
			}
			t.Errorf("Should get %s(OK), Got: %s", NotRun, status)
		}
		fmt.Println("NotRun status: OK")
	}
}

func TestPair(t *testing.T) {
	client := MakeClientNode("localhost", "8082", 2, 0)
	server := MakeServerNode("localhost", "8082", 2, 0)
	go RunNode(server)
	go RunNode(client)
	for _, n := range []NodeInterface{server, client} {
		status, err := WaitForStatus(n, Connected, timeOut)
		if err != nil {
			t.Fatalf("Error: %s; node %s", err, n)
		}
		if *status != Connected {
			t.Fatalf("Need Connected, got %s; node %s", *status, n)
		}
	}
	defer func() {
		for _, node := range []NodeInterface{client, server} {
			status, err := StopNode(node, timeOut)
			if err != nil {
				t.Fatal(err.Error())
			}
			fmt.Printf("Stopped, status = %s", *status)
		}
	}()
	payload := []byte("\x00\x0afdfdfdfdfd")
	client.GetIn() <- &payload
	msg := <-server.GetOut()
	if string(*msg) != string(payload) {
		t.Errorf("%x != %x", *msg, payload)
	}
}

func TestConnSropInPair(t *testing.T) {
	client := MakeClientNode("localhost", "8082", 2, 0)
	server := MakeServerNode("localhost", "8082", 2, 0)
	go RunNode(server)
	go RunNode(client)
	for _, n := range []NodeInterface{server, client} {
		status, err := WaitForStatus(n, Connected, timeOut)
		if err != nil {
			t.Fatalf("Error: %s; node %s", err, n)
		}
		if *status != Connected {
			t.Fatalf("Need Connected, got %s; node %s", *status, n)
		}
	}
	status, err := StopNode(client, timeOut)
	if err != nil || *status != NotRun {
		if err != nil {
			t.Fatal(err.Error())
		}
		t.Fatalf("Final status: %s\n", *status)
	}
	status, err = WaitForStatus(server, Connecting, timeOut)
	if err != nil || *status != Connecting {
		if err != nil {
			t.Fatal(err.Error())
		}
		t.Fatalf("Final status: %s\n", *status)
	}
	go RunNode(client)
	status, err = WaitForStatus(server, Connected, timeOut)
	if err != nil || *status != Connected {
		if err != nil {
			t.Fatal(err.Error())
		}
		t.Fatalf("Final statuses: %s\n", *status)
	}
}

func TestHaltConn(t *testing.T) {
	cmd := exec.Command("python", "./sock.py", "localhost", "8084")
	node := MakeServerNode("localhost", "8084", 2, 0)
	go RunNode(node)
	WaitForStatus(node, Connecting, timeOut)
	defer StopNode(node, timeOut)
	cmd.Start()
	if _, err := WaitForStatus(node, Connected, timeOut); err != nil {
		t.Fatalf("Errors: %s", err.Error())
	}
	if err := cmd.Process.Kill(); err != nil {
		t.Fatal(err.Error())
	}
	if status, err := WaitForStatus(node, Connecting, timeOut); err != nil {
		if err != nil {
			t.Errorf("Errror: %s", err.Error())
		}
		if status != nil {
			t.Errorf("Status: %s", *status)
		}
	}
}
