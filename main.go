package main

import (
	"fmt"

	"bitbucket.org/atomikin/proxy/db"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	dbConn, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("Connection to DB failed")
	}
	dbConn.AutoMigrate(&db.Connection{})
	cn := db.Connection{
		SutHost:      "localhost",
		SutPort:      8081,
		SutInitiator: true,
		HeaderLength: 2,
		HeaderType:   0,
	}
	dbConn.Create(
		&cn,
	)
	var c db.Connection
	fmt.Printf("ID = %d", cn.ID)
	dbConn.First(&c, 100)
	fmt.Println(c)
}
