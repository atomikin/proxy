# building the proxy

clean:
	[ -d "bin" ] && rm -r ./bin > /dev/null 2>&1 || exit 0

build-db: 
	go build ./db

install-db: build-db
	go install ./db

build-tcp: install-db
	go build ./tcp

install-tcp: build-tcp
	go install ./tcp

test-tcp:
	go clean -testcache
	go test ./tcp -count=1

build-rabbit: install-db
	go build ./rabbit

install-rabbit: build-rabbit
	go install ./rabbit

build-api: install-db install-tcp
	go build ./api

install-api: build-api
	go install ./api

build-app: install-api install-db install-tcp
	go build -o ./bin/proxy
	